package u02
import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import Lab02._
import u02.Lab02.Shape._
class Lab02Test {


  @Test def negZero() = {
    val isZero: Int => Boolean = _==0
    val negIsZero = negGeneric(isZero)
    assertTrue(negIsZero(10));
    assertFalse(negIsZero(0))
  }

  @Test def negIsOdd()= {
    val isOdd: Int => Boolean = _%2!=0
    val negIsOdd = negGeneric(isOdd)
    assertTrue(negIsOdd(2))
    assertFalse(negIsOdd(3))
  }

  @Test def negIsEmpty= {
    val isEmpty: String => Boolean = _==""
    val negIsEmpty = negGeneric(isEmpty)
    assertTrue(negIsEmpty("I'M NOT EMPTY"))
    assertFalse(negIsEmpty(""))
  }

  @Test def xyzGreater() = {
    assertTrue(p3(4)(5)(6))
    assertFalse(p3(5)(5)(3))
    assertTrue(p4(3,4,5))
    assertFalse(p4(5,5,3))
  }

  @Test def composeIntFunction() = {
    assertEquals(9, functionCompose[Int](_-1, _*2)(5))
    assertNotEquals(2, functionCompose[Int](_-1, _*2)(5))
  }

  @Test def composeStringFunction() = {
    assertEquals("Stringa Aggiunto a capo Aggiunta Stringa 1", functionCompose[String](_+" Aggiunta Stringa 1", _+" Aggiunto a capo")("Stringa"))
  }


  @Test def fibonacci() = {
    assertEquals(144, fibonacciNumber(12))
    assertNotEquals(12, fibonacciNumber(4))
    assertEquals(1, fibonacciNumber(1))
  }


  @Test def square() = {
    val square: Shape = Square(5)
    assertEquals(25, area(square))
    assertEquals(20, perimeter(square))
  }

  @Test def circle() = {
    val circle: Shape = Circle(5)
    assertEquals(Math.PI*5*5, area(circle))
    assertEquals(Math.PI*5*2, perimeter(circle))
  }

  @Test def rectangle() = {
    val rectangle: Shape = Rectangle(10, 15)
    assertEquals(150, area(rectangle))
    assertEquals(50, perimeter(rectangle))
  }

  @Test def filterOption() = {
    import Optionals.Option._
    assertEquals(None(), filter(Some(5))(_ < 2))
    assertEquals(Some(5), filter(Some(5))(_ > 2))
  }

  @Test def mapOption() = {
    import Optionals.Option._
    assertEquals(None(), map[Int, Boolean](None())( _ > 2))
    assertEquals(Some("FOUND"), map[Int, String](Some(5))( _ => "FOUND"))
    assertEquals(Some(true), map[Int, Boolean](Some(5))( _ > 2))
  }

  @Test def map2Option() = {
    import Optionals.Option._
    assertEquals(Some(25), map2[Int, Int](Some(10))(Some(15))((x,y) => x + y ))
    assertEquals(None(),  map2[Int, Int](Some(10))(None())((x,y) => x + y ))
    assertEquals(Some("25"),  map2[Int, String](Some(10))(Some(15))((x,y) => (x + y ).toString()))
    assertEquals(None(),  map2[Int, Int](None())(None())((x,y) => x + y ))


  }




}
