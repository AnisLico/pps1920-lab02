package u02


object Lab02 extends  App {

  /* Exercise 3a*/
  def parity(n : Int) = n match {
    case n if n % 2 == 0 => "even"
    case _ => "odd"
  }

  println(parity(5))

  val f: Int => String = {
    case n if n% 2 == 0 => "even"
    case _ => "odd"
  }

  println(f(10))


  /* Exercise 3b*/
  val empty: String => Boolean = _==""
  val negF: (String => Boolean) => (String => Boolean) = f => !f(_)
  val negEmpty = neg(empty)
  println(negEmpty("adgdsfg"))

  def neg(f: String => Boolean): String => Boolean = !f(_)

  /* Exercise 3c*/
  def negGeneric[T](f:T => Boolean): T => Boolean = !f(_)

  /* Exercise 4*/
  val p1 : Double => Double => Double => Boolean = x => y => z => x<=y && y<=z
  val p2  = (x: Double, y: Double, z: Double) => x<=y && y<=z
  def p3 (x : Double) (y: Double) (z: Double): Boolean =  x<=y && y<=z
  def p4 (x : Double, y: Double, z: Double): Boolean = x<=y && y<=z

  /* Exercise 5*/
  def functionCompose[T](f: T => T, g: T => T) : (T => T) = x => f(g(x))
  println(functionCompose[Int](_-1, _*2)(5))

  /* Exercise 6*/
  // @annotation . tailrec
  def fibonacciNumber(n: Int): Int = n match  {
    case 0 => 0
    case 1 => 1
    case _ => fibonacciNumber(n-1) + fibonacciNumber(n-2)
  }

  /* Exercise 7*/

  trait Shape
  object Shape{
    case class Rectangle(h: Double, w: Double) extends Shape
    case class Circle(r: Double) extends Shape
    case class Square(l: Double) extends Shape

    def perimeter(shape : Shape): Double = shape match {
      case Square(l) => l * 4
      case Rectangle(h,w) => h*2+ w*2
      case Circle(r) => Math.PI*2*r
    }

    def area(shape: Shape): Double = shape match {
      case Square(x) => x * x
      case Rectangle(h, w) => h*w
      case Circle(r) => Math.PI*r*r
    }
  }


  /* Exercise 8*/
  import Optionals.Option
  def filter[A](option: Option[A]) (predicate: A => Boolean) : Option[A] = option match {
    case Option.Some(a) => predicate(a) match {
      case true => Option.Some(a)
      case _ => Option.None()
    }
    case _ => Option.None()
  }

  def map[A, B](option: Option[A])(map: A => B) : Option[B] = option match {
    case Option.Some(a: A) => Option.Some(map(a))
    case _ => Option.None()
  }

  def map2[A, B](option1: Option[A])(option2: Option[A]) (map: (A, A) => B) : Option[B] = (option1,option2) match {
    case (Option.Some(a: A), Option.Some(b: A)) =>  Option.Some(map(a,b))
    case _ => Option.None()
  }









}
